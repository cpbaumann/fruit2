<?php


Class Log{

    public static function write($msg){
    
       
        $themsg = '# ' . date('d.m.Y H:i:s') . " # " . $msg . "\r\n";
        $res = error_log($themsg, 3, LOG_DIR);

        if(false === $res) echo 'error in error_log';
    }

}
