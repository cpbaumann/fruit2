<?php
/*

"id":"0E2C68G78EVMM917V91DXBR2RQ",  // meta data
"value":"2.0",
"feed_id":887173,                   // meta data
"feed_key":"te3",                   // meta data
"created_at":"2019-01-14T20:29:01Z",
"location":null,
"lat":null,
"lon":null,
"ele":null,
"created_epoch":1547497741,
"expiration":"2019-03-15T20:29:01Z"  // meta data


"value":"2.0",
"created_at":"2019-01-14T20:29:01Z",
"location":null,
"lat":null,
"lon":null,
"ele":null,
"created_epoch":1547497741,
*/

class Database {

	function __construct() {

		try	{

			$path = realpath($_SERVER['DOCUMENT_ROOT'] . '/../db' . '/sqlite3.data.fruits.db');

			if(!file_exists($path))	{
				exit( 'db exists not in filesystem. run setup first.');
			}

			$this->dbh = new PDO("sqlite:$path");

			#set the PDO error mode to exception
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


			$this->tables = ['tbl_te3','tbl_te7','tbl_fu7','tbl_fu3'];
		}

		catch (PDOException $e) {
			print "Error: " . $e->getMessage() . "<br />";
			exit();
		}
	}

	function select($source, $condition) {

		if (!in_array($source, $this->tables)) exit('tbl name ist not allowed.');

		$sql = "SELECT * FROM $source WHERE created_epoch BETWEEN :value_from AND :value_to ORDER BY created_epoch ASC";

		try {
			$stmt = $this->dbh->prepare($sql);
		}

		catch (PDOException $e){
			print "Error prepare: " . $e->getMessage() . "<br />";
			return false;
		}

		$stmt->bindValue(':value_from', $condition['value_from'], PDO::PARAM_INT);
		$stmt->bindValue(':value_to', $condition['value_to'], PDO::PARAM_INT);

		try {
			$stmt->execute();
		}

		catch (PDOException $e){
			print "Error execute: " . $e->getMessage() . "<br />";
			return false;
		}
		return $stmt->fetchAll();
	}

	function insert($source,$values) {
		
		if (!in_array($source, $this->tables)) exit('tbl name ist not allowed.');

		$sql = "INSERT INTO $source
				(
					value,
					created_at, 
					location,
					lat,
					lon,
					ele,
					created_epoch 
				)
				VALUES
				(
					:value,
					:created_at, 
					:location,
					:lat,
					:lon,
					:ele,
					:created_epoch 
				)";

		try {
			$stmt = $this->dbh->prepare($sql);
		}

		catch (PDOException $e){
			print "Error prepare: " . $e->getMessage() . "<br />";
			return false;
		}

		$stmt->bindValue(':value', $values['value'], PDO::PARAM_STR);
		$stmt->bindValue(':created_at', $values['created_at'], PDO::PARAM_STR);
		$stmt->bindValue(':location', $values['location'], PDO::PARAM_STR);

		$stmt->bindValue(':lat', $values['lat'], PDO::PARAM_STR);
		$stmt->bindValue(':lon', $values['lon'], PDO::PARAM_STR);
		$stmt->bindValue(':ele', $values['ele'], PDO::PARAM_STR);
		$stmt->bindValue(':created_epoch', $values['created_epoch'], PDO::PARAM_INT);

		try {
			$stmt->execute();
		}

		catch (PDOException $e) {
			print "Error execute: " . $e->getMessage() . "<br />";
			return false;
		}
	}
}