<?php


if (!extension_loaded('curl')) {
        echo 'curl extension not loaded.';
}


    #http://php.net/manual/de/function.curl-getinfo.php

    Class GetPage{

        public static function request($url){

            $data = [];
            $data['time_start'] = date('d.m.Y H:i:s');

            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLINFO_HEADER_OUT , 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            include realpath($_SERVER['DOCUMENT_ROOT'] . '/../db' . '/key-config.php');

            $headers = [
                $X_AIO_Key
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


            #curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:57.0) Gecko/20100101 Firefox/57.0');

            $content = curl_exec($ch);
            $data['curl_info'] = curl_getinfo($ch);


            if(!curl_errno($ch)){
                $data['content'] = $content;
            }

            if($errno = curl_errno($ch)) {
                $data['curl_error'] = curl_strerror($errno);
            }

           
            curl_close($ch);
            return $data;
        }   
    }