<?php

header("Access-Control-Allow-Origin: *");

#header('Content-Type: text/csv');

function __autoload($class_name) {
    require_once "./../class/" . $class_name . ".php";
}

include '../inc/config.php';


$tbl1_default = 'te3';
$tbl2_default = 'te7';

if(isset($_GET['tbl1']) and !empty($_GET['tbl1'])) {
	if(preg_match("/^[a-z0-9]{1,4}$/", $_GET['tbl1'])) {
		$tbl1 = $_GET['tbl1'];
	}else{
		$tbl1 = $tbl1_default;
	} 
}else{
	$tbl1 = $tbl1_default;
}


if(isset($_GET['tbl2']) and !empty($_GET['tbl2'])) {
	if(preg_match("/^[a-z0-9]{1,4}$/", $_GET['tbl2'])) {
		$tbl2 = $_GET['tbl2'];
	}else{
		$tbl2 = $tbl2_default;
	} 
}else{
	$tbl2 = $tbl2_default;
}	



$t_default = -1;  // yesterday

if(isset($_GET['t']) and !empty($_GET['t'])) {
	if(preg_match('/^-?[0-9]{1,3}$/', $_GET['t'])) {
		$t = $_GET['t'];
	}else{
		$t = $t_default;
	} 
}else{
	$t = $t_default;
}		

#date_default_timezone_set('UTC');
date_default_timezone_set('Europe/London');


$value_from = 0 + $t;
$value_to   = 1 + $t;


# echo 'GET t' . $t . ' from ' . $value_from  . ' to ' . $value_to ;

$db =  new Database;

$condition['value_from'] = mktime(0, 0, 0, date("m"), date("d") + $value_from, date("Y"));
$condition['value_to'] = mktime(0, 0, 0, date("m"), date("d") + $value_to, date("Y"));

$res1 = $db->select('tbl_' . $tbl1, $condition);

if (count($res1) == 0) {
	Log::write('no data from tbl_' . $tbl1 . ' ' . print_r($condition,true));
	#exit('no data from tbl_tbl_' . $tbl1 . print_r($condition,true));
} 

$res2 = $db->select('tbl_' . $tbl2, $condition);

if (count($res2) == 0) {
	Log::write('no data from tbl_' . $tbl2 . ' ' . print_r($condition,true));
	#exit('no data from tbl_tbl_' . $tbl2 . print_r($condition,true));
} 



$data = NULL;


// we need the longer data set at first position to avoid kommata without values in cvs
// outerwise this will cause uncontolled lines 

if (count ($res1) >  count ($res2)) {

	$data =   'createdate1'  . ',' .  'thevalue1'. ',' .'createdate2'  . ',' .  'thevalue2'  . "\n"; 

	foreach ($res1 as $key => $value) {

		if ($res1[$key]['created_at'] != '') $d1[$key]  = $res1[$key]['created_at'] . "," . $res1[$key]['value'];

		if ($res2[$key]['created_at'] != ''){
			$d2[$key]  = "," . $res2[$key]['created_at'] . "," . $res2[$key]['value'];
		}else{
			$d2[$key] = '';
		}  
		$data .= $d1[$key] . $d2[$key] . "\n"; 
	}
}

if (count ($res1) <  count ($res2)) {

	$data =   'createdate2'  . ',' .  'thevalue2'. ',' .'createdate1'  . ',' .  'thevalue1'  . "\n"; 

	foreach ($res2 as $key => $value) {

		if ($res2[$key]['created_at'] != '') $d1[$key]  = $res2[$key]['created_at'] . "," . $res2[$key]['value'];

		if ($res1[$key]['created_at'] != ''){
			$d2[$key]  = "," . $res1[$key]['created_at'] . "," . $res1[$key]['value'];
		}else{
			$d2[$key] = '';
		}  
		$data .= $d1[$key] . $d2[$key] . "\n"; 
	}
}




Log::write('one view of ' . $tbl1 . ' and ' . $tbl2 . ' value_from ' . $condition['value_from'] . ' value_to ' . $condition['value_to']  . ' ');

echo $data; 
?>