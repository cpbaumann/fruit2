<?php
/*
*
*	utf8 only
*	no regs!
*
*	realpath
*
*/


class ShowLog{

	private $log_file;

	public function read($log_file){

		$this->log_file = $log_file;

		try{
			if( file_exists( realpath($this->log_file) ) ){

				$this->log_file = realpath($this->log_file);

				if ($handel = fopen($this->log_file, 'r')){
					$this->code = fread($handel, filesize($this->log_file));
					fclose($handel);
					return $this->code;
				}else{
					throw new Exception('Fail of fopen (' . $this->log_file . ')');
				}

			}else{
				throw new Exception('Exception: Log file (' . $this->log_file . ') not found');
			}
		}

		catch (Exception $e){
			Log::write($e->getMessage(), 'dev');
			Log::write(" in " . $e->getFile() . ", line: " . $e->getLine(), 'dev');
		}

	}



	public function readtail($log_file, $num_lines, $buffer_size = 1024){

		$this->log_file = $log_file;
		$this->num_lines = $num_lines;
		$this->buffer_size = $buffer_size;

		try{
			if( file_exists( realpath($this->log_file) ) ){

				$this->log_file = realpath($this->log_file);

				if ($handel = fopen($this->log_file, 'r')){
					
					# http://stackoverflow.com/questions/11068203/php-retrieving-lines-from-the-end-of-a-large-text-file
					fseek($handel, 0, SEEK_END);
					$pos = ftell($handel);

					$input = '';
 					$line_count = 0;

					while ($line_count < $this->num_lines + 1) {

						// read the previous block of input
    					$read_size = $pos >= $this->buffer_size ? $this->buffer_size : $pos;

    					fseek($handel, $pos - $read_size, SEEK_SET);

    					// prepend the current block, and count the new lines
    					$input = fread($handel, $read_size) . $input;
    					$line_count = substr_count(ltrim($input), "\n");

    					// if $pos is == 0 we are at start of file
    					$pos -= $read_size;
    					if (!$pos) break;
  					}

  					$this->code  = array_slice(explode("\n", rtrim($input)), -$this->num_lines);
					fclose($handel);
					return $this->code;

				}else{
					throw new Exception('Fail of fopen (' . $this->log_file . ')');
				}

			}else{
				throw new Exception('Exception: Log file (' . $this->log_file . ') not found');
			}
		}

		catch (Exception $e){
			Log::write($e->getMessage(), 'dev');
			Log::write(" in " . $e->getFile() . ", line: " . $e->getLine(), 'dev');
		}

	}



}