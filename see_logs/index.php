<?php

function __autoload($class_name) {
	require_once "./class/" . $class_name . ".php";
}


$lines_default = 200;

if(isset($_GET['lines']) and !empty($_GET['lines'])) {
	if(preg_match("/^[1-9]{1}[0-9]{0,}$/", $_GET['lines'])) {
		$lines = $_GET['lines'];
	}else{
		$lines = $lines_default;
	} 
}else{
	$lines = $lines_default;
}		


$log = new ShowLog;


if($_SERVER['HTTP_HOST'] === 'localhost'){
    $log_dir = realpath ($_SERVER['DOCUMENT_ROOT'] . '/../logs') . '/fruit.log';
}else{
    $log_dir = realpath ($_SERVER['DOCUMENT_ROOT'] . '/../logs') . '/fruit.log';
}


$c = $log->readtail($log_dir, $lines);

?>
<!DOCTYPE html>
<html> 
<head>
<meta charset="utf-8">
<title>logs</title>
<style>
body {
	background: #fff;
	margin: 0;
	padding: 1em;
	font-family: Courier;
	font-size: .75em;
}

pre{
	font-family: Courier;
	font-size: 1em;
}
</style>
</head>
<body>
	<p>You see the last <?php echo $lines_default; ?> lines by <a href="?lines=<?php echo $lines_default; ?>">default</a>. More:
	
	<a href="?lines=<?php echo $lines_default*8; ?>"><?php echo $lines_default*8; ?></a> | 
	<a href="?lines=<?php echo $lines_default*16; ?>"><?php echo $lines_default*16; ?></a> | 
	<a href="?lines=<?php echo $lines_default*32; ?>"><?php echo $lines_default*32; ?></a></p>
	<pre>
<?php 
foreach($c as $k => $v):
echo $v;
endforeach; 
?>
	</pre>
</body>
</html>