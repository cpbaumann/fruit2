<?php

foreach(PDO::getAvailableDrivers() as $driver) {
	echo 'PDO driver: ', $driver ,'<br />';
}


try {

	/*** connect to SQLite database ***/
	$path = realpath($_SERVER['DOCUMENT_ROOT'] . '/../db' . '/sqlite3.data.fruits.db');

	if(!file_exists($path))	{
		exit( 'db exists not in filesystem. run setup first.');
	}

	$dbh = new PDO("sqlite:$path");

	#set the PDO error mode to exception
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}


catch (PDOException $e) {
	die($e->getMessage());
}



try {
	/*** begin the transaction ***/
	$dbh->beginTransaction();


 	#----------- te3

	// Try to find a table named 'tbl_te3'
	$q1 = $dbh->query("SELECT name FROM sqlite_master WHERE type = 'table'" .  " AND name = 'tbl_te3'");
 

	// If the query didn't return a row, then create the table
	if ($q1->fetch() === false) {

		$table = "CREATE TABLE tbl_te3
		(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			value TEXT,
			created_at TEXT,
			location TEXT,
			lat TEXT,
			lon TEXT,
			ele TEXT,
			created_epoch INTEGER
		)";
		$dbh->exec($table);

		echo 'table was created <pre>', $table, '</pre>';
		
	}else{
		echo 'table tbl_te3 exists <br />';
	}


	#----------------   te7

	// Try to find a table named 'tbl_te7'
	$q2 = $dbh->query("SELECT name FROM sqlite_master WHERE type = 'table'" .  " AND name = 'tbl_te7'");
 

	if ($q2->fetch() === false) {

		$table = "CREATE TABLE tbl_te7
		(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			value TEXT,
			created_at TEXT,
			location TEXT,
			lat TEXT,
			lon TEXT,
			ele TEXT,
			created_epoch INTEGER
		)";
		$dbh->exec($table);

		echo 'table was created <pre>', $table, '</pre>';
		
	}else{
		echo 'table tbl_te7 exists <br />';
	}


	#----------------   fu3

	// Try to find a table named 'tbl_fu3'
	$q3 = $dbh->query("SELECT name FROM sqlite_master WHERE type = 'table'" .  " AND name = 'tbl_fu3'");
 

	// If the query didn't return a row, then create the table
	if ($q3->fetch() === false) {

		$table = "CREATE TABLE tbl_fu3
		(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			value TEXT,
			created_at TEXT,
			location TEXT,
			lat TEXT,
			lon TEXT,
			ele TEXT,
			created_epoch INTEGER
		)";
		$dbh->exec($table);

		echo 'table was created <pre>', $table, '</pre>';


		
	}else{
		echo 'table tbl_fu3 exists <br />';
	}


	#----------------  fu7

	// Try to find a table named 'tbl_fu7'
	$q4 = $dbh->query("SELECT name FROM sqlite_master WHERE type = 'table'" .  " AND name = 'tbl_fu7'");
 

	// If the query didn't return a row, then create the table
	if ($q4->fetch() === false) {

		$table = "CREATE TABLE tbl_fu7
		(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			value TEXT,
			created_at TEXT,
			location TEXT,
			lat TEXT,
			lon TEXT,
			ele TEXT,
			created_epoch INTEGER
		)";
		$dbh->exec($table);

		echo 'table was created <pre>', $table, '</pre>';


		
	}else{
		echo 'Database OK, table tbl_fu7 exists <br />';
	}


	$dbh->commit();


}
catch(PDOException $e) {
	/*** roll back the transaction if we fail ***/
	$dbh->rollback();

	/*** echo the sql statement and error message ***/
	echo $sql . '<br />' . $e->getMessage();
	echo $dbh->errorCode();
}
?>
