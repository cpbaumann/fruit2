BEGIN TRANSACTION;
DROP TABLE IF EXISTS `tbl_te7`;
CREATE TABLE IF NOT EXISTS `tbl_te7` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`value`	TEXT,
	`created_at`	TEXT,
	`location`	TEXT,
	`lat`	TEXT,
	`lon`	TEXT,
	`ele`	TEXT,
	`created_epoch`	INTEGER
);
DROP TABLE IF EXISTS `tbl_te3`;
CREATE TABLE IF NOT EXISTS `tbl_te3` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`value`	TEXT,
	`created_at`	TEXT,
	`location`	TEXT,
	`lat`	TEXT,
	`lon`	TEXT,
	`ele`	TEXT,
	`created_epoch`	INTEGER
);
DROP TABLE IF EXISTS `tbl_fu7`;
CREATE TABLE IF NOT EXISTS `tbl_fu7` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`value`	TEXT,
	`created_at`	TEXT,
	`location`	TEXT,
	`lat`	TEXT,
	`lon`	TEXT,
	`ele`	TEXT,
	`created_epoch`	INTEGER
);
DROP TABLE IF EXISTS `tbl_fu3`;
CREATE TABLE IF NOT EXISTS `tbl_fu3` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`value`	TEXT,
	`created_at`	TEXT,
	`location`	TEXT,
	`lat`	TEXT,
	`lon`	TEXT,
	`ele`	TEXT,
	`created_epoch`	INTEGER
);
COMMIT;
