<?php


/*
 *
 *	PDO SQLite create a database and insert test data
 *
 *
 */

 


foreach(PDO::getAvailableDrivers() as $driver) {
	echo 'PDO driver: ', $driver ,'<br />';
}


try {
	/*** connect to SQLite database ***/
	$dbh = new PDO("sqlite:sqlite3.data.fruits.db");
 

	 /*** set the PDO error mode to exception ***/
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}

catch (PDOException $e) {
	die($e->getMessage());
}



try {
	/*** begin the transaction ***/
	$dbh->beginTransaction();


	// Try to find a table named 'koordinates'
	$q = $dbh->query("SELECT name FROM sqlite_master WHERE type = 'table'" .  " AND name = 'tbl_te3'");


 

	// If the query didn't return a row, then create the table
	// and insert the data
	if ($q->fetch() === false) {



		/***  

						value,
						created_at, 
						location,
						lat,
						lon,
						ele,
						created_epoch 
		***/

		/*** CREATE table statements http://www.sqlite.org/autoinc.html ***/

		$table = "CREATE TABLE tbl_te3
		(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			value TEXT,
			created_at TEXT,
			location TEXT,
			lat TEXT,
			lon TEXT,
			ele TEXT,
			created_epoch INTEGER
		)";
		$dbh->exec($table);




		/*** echo a message to say the database was created ***/
		echo 'database and table was created <pre>', $table, '</pre>';

		/*
		"created_at":"2019-01-16T12:23:28Z",
		"location":null,
		"lat":null,
		"lon":null,
		"ele":null,
		"created_epoch":1547641408
		*/

		$insertsql = "INSERT INTO tbl_te3
		(
			value,
			created_at, 
			location,
			lat,
			lon,
			ele,
			created_epoch

		) VALUES (
			'2,7',
			'2019-01-16T12:23:28Z',
			'null',
			'null',
			'null',
			'null',
			'1547641408' 
		)";
		$dbh->exec($insertsql);
		echo 'Testdata entered successfully <pre>' . $insertsql . '</pre><br />';
	}else{
		echo 'Database OK, tabele tbl_news exists <br />';
	}




	/*** The SQL SELECT statement ***/
	$sql = "SELECT * FROM tbl_te3";

	echo 'result of ' . $sql . '<br /> ------------------------- <br />';

	foreach ($dbh->query($sql) as $row):
		for($i = 0; $i <= 7; $i++):
			print $row[$i] . ' | ';
		endfor;
	print  '<br /> ------------------------- <br />';
	endforeach;


	$dbh->commit();


}
catch(PDOException $e) {
	/*** roll back the transaction if we fail ***/
	$dbh->rollback();

	/*** echo the sql statement and error message ***/
	echo $sql . '<br />' . $e->getMessage();
	echo $dbh->errorCode();
}
?>
