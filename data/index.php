<?php
header("Access-Control-Allow-Origin: *");

#header('Content-Type: text/csv');

function __autoload($class_name) {
    require_once "./../class/" . $class_name . ".php";
}

include '../inc/config.php';


$tbl_default = 'te3';

if(isset($_GET['tbl']) and !empty($_GET['tbl'])) {
	if(preg_match("/^[a-z0-9]{1,4}$/", $_GET['tbl'])) {
		$tbl = $_GET['tbl'];
	}else{
		$tbl = $tbl_default;
	} 
}else{
	$tbl = $tbl_default;
}	


$t1_default = -1;  // begin day from today

if(isset($_GET['t1']) and !empty($_GET['t1'])) {
	if(preg_match('/^-?[0-9]{1,3}$/', $_GET['t1'])) {
		$t1 = $_GET['t1'];
	}else{
		$t1 = $t1_default;
	} 
}else{
	$t1 = $t1_default;
}		


$t2_default = 0;  // end day from today

if(isset($_GET['t2']) and !empty($_GET['t2'])) {
	if(preg_match('/^-?[0-9]{1,3}$/', $_GET['t2'])) {
		$t2 = $_GET['t2'];
	}else{
		$t2 = $t2_default;
	} 
}else{
	$t2 = $t2_default;
}		



#date_default_timezone_set('UTC');
date_default_timezone_set('Europe/London');

$value_from = $t1;
$value_to   = $t2;


# echo 'GET t1' . $t1 . 'GET t2' . $t2 . ' from ' . $value_from  . ' to ' . $value_to ;

$db =  new Database;

$condition['value_from'] = mktime(0, 0, 0, date("m"), date("d") + $value_from, date("Y"));
$condition['value_to'] = mktime(0, 0, 0, date("m"), date("d") + $value_to, date("Y"));

$res = $db->select('tbl_' . $tbl, $condition);

if (count($res) == 0) {
	Log::write('no data from tbl_' . $tbl . ' ' . print_r($condition,true));
	echo 'no data from tbl_tbl_' . $tbl . ' ' . print_r($condition,true);
} 

$data = NULL;

$data =   'createdate'  . ',' .  'thevalue'  . "\n"; 

foreach ($res as $key => $value) {
  $data .= $value['created_at'] . "," . $value['value'] . "\n"; 
}

Log::write('one view of ' . $tbl . ' value_from ' . $condition['value_from'] . ' value_to ' . $condition['value_to']  . ' ');

echo $data; 
?>