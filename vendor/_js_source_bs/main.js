(function(){
  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.offcanvas-collapse').toggleClass('open');
  });
})();

(function(){
 var previousScroll = 0;

    $(window).scroll(function(){
       var currentScroll = $(this).scrollTop();
       if (currentScroll > previousScroll){
           console.log('down');
           $('.navbar').removeClass('is-visible');
           $('.navbar').addClass('is-hidden');
       } else {
          console.log('up');
          $('.navbar').removeClass('is-hidden');
          $('.navbar').addClass('is-visible');
       }
       previousScroll = currentScroll;
    });
})();

