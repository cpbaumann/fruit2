<?php

function __autoload($class_name) {
    require_once "./../class/" . $class_name . ".php";
}

include './../inc/config.php';


function import_feed_to_db($feed_key,$feed_id){

  $hn_yesterday =  mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
  $hn_today =  mktime(0, 0, 0, date("m"), date("d"), date("Y"));

  echo 'time code unixtime begin: ' . $hn_yesterday . ' time: ' . date("d.m.Y H:i:s", $hn_yesterday) . '<br>';
  echo 'time code unixtime end : ' . $hn_today . ' time: ', date("d.m.Y H:i:s", $hn_today) . '<br>';

  $import = true;

  $db =  new Database;
  $condition['value_from'] = $hn_yesterday;
  $condition['value_to'] = $hn_today; 
  $res = $db->select('tbl_' . $feed_key, $condition);

  if (count($res) > 0) {

    Log::write('job is allready done for tbl_' . $feed_key . ' We have ' . count($res) . ' entries for yesterday.');
    echo('job is allready done for tbl_' . $feed_key . ' We have ' . count($res) . ' entries for yesterday.<br>');

    $import = false;
  } 
 

  $url  = 'https://io.adafruit.com/api/v2/Zambina/feeds/' . $feed_id . '/data';
  $data = GetPage::request($url);
  $msg  =  'feed named as ' . $feed_key . ', ' 
        . ' response_total_time: ' . $data ['curl_info']['total_time'] 
        . ', ' . 'code: ' . $data['curl_info']['http_code'] . ' ' . $status_code[$data['curl_info']['http_code']]; 

  Log::write($msg);
  #Log::write($data['curl_info']);
  #print_r($data['curl_info']);
  echo $msg . '<br>';


  if ($data['curl_info']['http_code'] == '200') {
  
    if ($import === true) {

      $content = json_decode($data['content'], true);

      file_put_contents(LOG_JASON . 'fruit.' . $feed_key . '.jason', $data['content'], FILE_APPEND | LOCK_EX);

      foreach ($content as $key => $value) {

        if(($hn_yesterday <= $value['created_epoch']) and ($value['created_epoch'] < $hn_today)) {

          $values['value']      = $value['value'] ;
          $values['created_at'] = $value['created_at'] ;
          $values['location']   = $value['location'] ;
          $values['lat']        = $value['lat'] ;
          $values['lon']        = $value['lon'] ;
          $values['ele']        = $value['ele'] ;
          $values['created_epoch'] = $value['created_epoch'] ;

          $db->insert('tbl_' . $feed_key,$values); 

          echo 'import tbl_' . $feed_key . ': ' . print_r($values,true) . '<br>';
          #Log::write('tbl_' . $feed_key . ': ' . print_r($values,true));
        } 
      }
   
    } else {
      echo 'no import tbl_' . $feed_key . '<br>';
    }

  } else {
    Log::write('curl_info http_code != 200 for ' . $url );
    echo('no data for tbl_' . $feed_key);
  }
}


import_feed_to_db('te3','887173');
import_feed_to_db('te7','961695');
import_feed_to_db('fu7','961699');
import_feed_to_db('fu3','887179');
?>