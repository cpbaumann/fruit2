<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Europe/London');

$tbl = NULL;
$t1 = NULL;
$t2 = NULL;
$legend_y_axis = 'temperature';

$tbl_default = 'te3';

if(isset($_GET['tbl']) and !empty($_GET['tbl'])) {
  if(preg_match("/^[a-z0-9]{1,4}$/", $_GET['tbl'])) {
    $tbl = $_GET['tbl'];
  }else{
    $tbl = $tbl_default;
  } 
}else{
  $tbl = $tbl_default;
} 


$t1_default = -1;  // begin day from today

if(isset($_GET['t1']) and !empty($_GET['t1'])) {
  if(preg_match('/^-?[0-9]{1,3}$/', $_GET['t1'])) {
    $t1 = $_GET['t1'];
  }else{
    $t1 = $t1_default;
  } 
}else{
  $t1 = $t1_default;
}   


$t2_default = 0;  // end day from today

if(isset($_GET['t2']) and !empty($_GET['t2'])) {
  if(preg_match('/^-?[0-9]{1,3}$/', $_GET['t2'])) {
    $t2 = $_GET['t2'];
  }else{
    $t2 = $t2_default;
  } 
}else{
  $t2 = $t2_default;
}   

$days = -($t1 - $t2);

if($days == 1){
  $ticks = $days * 12;
}

if($days == 2){
  $ticks = $days * 4;
}

if( $days > 2){
  $ticks = $days * 2;
}

if( $days > 4){
  $ticks = $days * 1;
}

if( $days > 8){
  $ticks = abs($days / 3);
}

if( $days > 15){
  $ticks = abs($days / 4);
}

if( $days > 25){
  $ticks = abs($days / 6);
}

if( $tbl == 'te3' or $tbl == 'te7'){
  $legend_y_axis = 'temperature in celsius';
}

if( $tbl == 'fu3' or $tbl == 'fu7'){
  $legend_y_axis = 'luminous intensity';
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>fruit <?php echo $tbl ?></title>
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="/fruit2/css/bootstrap.css">
<style>  
.legend { 
  font-size: 1rem; 
  font-family:"Source Sans Pro";
}

.legend-small { 
  font-size: .725rem; 
  font-family:"Source Sans Pro";
}

.pointer { 
  cursor: pointer;
}

.line  {
  fill: none;
  stroke: rgb(253,126,20);
  stroke-width: 1px;
  opacity: 0;
}

.dot  {
  fill: rgb(253,126,20);
  opacity: 1;
}

.area {
  fill: rgb(253,126,20);
  opacity: 0;
}

svg{
  border: 1px rgba(0,0,0,0.2) solid;
  max-width: 100%;
}

#chart {
    overflow-x: auto;
}
</style>
</head>
<body>
<?php include '../inc/nav.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <h1><?php 
          if (($t2 - $t1) > 1) {
            $time_legend =  'sensor: ' . $tbl . ', period: ' . date("j.m.Y - ", strtotime( $t1 . ' days' ) ) . ' ' . date("j.m.Y", strtotime( $t2 . ' days' ) );  
            $timeFormat = '%d.%m %H:%M'; 
          }else{
            $time_legend =  'sensor: ' . $tbl . ', day: ' . date("j. F Y", strtotime( $t1 . ' days' ) );
            $timeFormat = '%H:%M';
          }
          echo $time_legend;
          ?></h1>
          <div id="chart"></div>
          <br><br>
        </div>
    </div>
</div>      
<script src="/fruit2/js/d3.v4.min.js"></script>
<script>
// https://bl.ocks.org/d3noob/119a138ef9bd1d8f0a8d57ea72355252 
// http://www.d3noob.org/2016/11/change-line-chart-into-scatter-plot-in.html
// https://bl.ocks.org/d3noob/4abb9dc578abf070fe62302282a29c41


// set the dimensions and margins of the graph
var margin = {top: 40, right: 50, bottom: 40, left: 70},
    width = 960 - margin.left - margin.right,
    height = 430 - margin.top - margin.bottom;

// parse the date / time 2018-12-25T11:09:34Z
var parseTime = d3.utcParse("%Y-%m-%dT%H:%M:%SZ");

// set the ranges
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

// define the line
var valueline = d3.line()
    .x(function(d) { return x(d.createdate); })
    .y(function(d) { return y(d.thevalue); });


// define the area
var area = d3.area()
    .x(function(d) { return x(d.createdate); })
    .y0(height)
    .y1(function(d) { return y(d.thevalue); });


// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("#chart").append("svg")
    //.attr("width", width + margin.left + margin.right)
    //.attr("height", height + margin.top + margin.bottom)
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 960 460")
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

// Get the data
d3.csv("http://datenschubse.de/fruit2/data/?tbl=<?php echo $tbl; ?>&t1=<?php echo $t1; ?>&t2=<?php echo $t2; ?>", function(error, data) {
  if (error) throw error;


console.log(data.length)

  if(data.length < 5) {
    console.log("File empty")

    svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .attr("class", "legend")
        .text("no data found!");
  } else {
    svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .attr("class", "legend")
        .text("<?php echo $time_legend; ?>");  
  }

  // format the data
  data.forEach(function(d) {
      d.createdate = parseTime(d.createdate);
      d.thevalue = +d.thevalue;
  });

  min = d3.min(data, function(d) { return d.thevalue; })
  max = d3.max(data, function(d) { return d.thevalue; })

  if(min > 0) {
    min = 0;
  } else {
     min = min - 1;
  }

  max = max + 1;

  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.createdate; }));
  y.domain([min, max]);


  // add the area
  svg.append("path")
      .data([data])
      .attr("class", "area")
      .attr("id", "area")
      .attr("d", area);


  // Add the valueline path.
  svg.append("path")
      .data([data])
      .attr("class", "line")
      .attr("id", "theLine")
      .attr("d", valueline);


  // Add the scatterplot
  svg.selectAll("dot")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot")
      .attr("r", 1)
      .attr("cx", function(d) { return x(d.createdate); })
      .attr("cy", function(d) { return y(d.thevalue); });
  

  // Add the X Axis // "%Y-%m-%d %H:%M:%S"
  svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x).tickFormat(d3.timeFormat("<?php echo $timeFormat; ?>"))
        .ticks(<?php echo $ticks; ?>))


  // Add the Y Axis
  svg.append("g")
      .call(d3.axisLeft(y));



  // text label for the x axis
  svg.append("text")  
      .attr("class","legend-small")           
      .attr("transform",
            "translate(" + (width/2) + " ," + (height + margin.top + 10) + ")")
      .style("text-anchor", "middle")
      .text("time");

  // text label for the y axis
  svg.append("text")
      .attr("class","legend-small")
      .attr("transform", "rotate(-90)")
      .attr("y", 10 - margin.left)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("<?php echo $legend_y_axis; ?>");      


  // Add the blue line title
  svg.append("text")
    .attr("x", 0)             
    .attr("y", height + margin.top + 10)    
    .attr("class", "legend-small pointer")       
    .on("click", function(){
      // Determine if current line is visible
      var activeLine = theLine.activeLine ? false : true;
      var newOpacityLine = activeLine ? 1 : 0;
      var newOpacityArea = activeLine ? 0.1 : 0;
      // Hide or show the elements
      d3.select("#theLine").style("opacity", newOpacityLine);
      d3.select("#area").style("opacity", newOpacityArea);
      // Update whether or not the elements are active
      theLine.activeLine = activeLine;
    })
    .text("show/hide line and area");  
});
</script>
<script src="/fruit2/vendor/jquery-3.2.1.slim.min.js"></script>
<script src="/fruit2/vendor/bootstrap.min.4.1.3.js"></script>
</body>
</html>