<?php

header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Europe/London');

$tbl1 = NULL;
$tbl2 = NULL;
$t = NULL;
$legend_y_axis = 'temperature';

$tbl1_default = 'te3';
$tbl2_default = 'te7';

if(isset($_GET['tbl1']) and !empty($_GET['tbl1'])) {
  if(preg_match("/^[a-z0-9]{1,4}$/", $_GET['tbl1'])) {
    $tbl1 = $_GET['tbl1'];
  }else{
    $tbl1 = $tbl1_default;
  } 
}else{
  $tbl1 = $tbl1_default;
}


if(isset($_GET['tbl2']) and !empty($_GET['tbl2'])) {
  if(preg_match("/^[a-z0-9]{1,4}$/", $_GET['tbl2'])) {
    $tbl2 = $_GET['tbl2'];
  }else{
    $tbl2 = $tbl2_default;
  } 
}else{
  $tbl2 = $tbl2_default;
} 



$t_default = -1;  // yesterday

if(isset($_GET['t']) and !empty($_GET['t'])) {
  if(preg_match("/^-?[0-9]{1,3}$/", $_GET['t'])) {
    $t = $_GET['t'];
  }else{
    $t = $t_default;
  } 
}else{
  $t = $t_default;
}   


if( $tbl1 == 'te3' or $tbl1 == 'te7'){
  $legend_y_axis = 'temperature in celsius';
}

if( $tbl1 == 'fu3' or $tbl1 == 'fu7'){
  $legend_y_axis = 'luminous intensity';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>chart <?php echo $tbl1 . '/' . $tbl2; ?></title>
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="/fruit2/css/bootstrap.css">
<style>  
.legend { 
  font-size: 1rem; 
  font-family:"Source Sans Pro";
}

.legend-small { 
  font-size: .725rem; 
  font-family:"Source Sans Pro";
}

.pointer{
  cursor: pointer;
  color: black;
}

.line  {
  fill: none;
  stroke: #fd7e14;
  opacity: 0;
  stroke-width: 1px;
}

.line1  {
  fill: none;
  stroke: orange;
  opacity: 0;
  stroke-width: 1px;
}

.line2  {
  fill: none;
  stroke: red;
  opacity: 0;
  stroke-width: 1px;
}
 
.dot1  {
  fill: orange;
}

.dot2  {
  fill: red;
}

svg{
  border: 1px rgba(0,0,0,0.4) solid;
  max-width: 100%;
}

#chart{
  overflow-x: auto;
}
</style>
</head>
<body>
<?php include '../inc/nav.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <h1><?php echo $tbl1 . '/' .  $tbl2   . ' ' . date("j. F Y", strtotime( $t . ' days' ) );?></h1>
          <div id="chart"></div>
          <br><br>
        </div>
    </div>
</div>      
<script src="/fruit2/js/d3.v4.min.js"></script>
<script>
// https://bl.ocks.org/d3noob/4db972df5d7efc7d611255d1cc6f3c4f
// set the dimensions and margins of the graph
var margin = {top: 40, right: 40, bottom: 80, left: 80},
    width = 960 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom;

// 2018-12-25T11:09:34Z
var parseTime = d3.timeParse("%Y-%m-%dT%H:%M:%SZ");

// set the ranges
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

var valueline1 = d3.line()
    .x(function(d) { return x(d.createdate1); })
    .y(function(d) { return y(d.thevalue1); });

// define the 2nd line
var valueline2 = d3.line()
    .x(function(d) { return x(d.createdate2); })
    .y(function(d) { return y(d.thevalue2); });

// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("#chart").append("svg")
    //.attr("width", width + margin.left + margin.right)
    //.attr("height", height + margin.top + margin.bottom)
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 960 460")
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

// Get the data
d3.csv("http://datenschubse.de/fruit2/data-multiline/?tbl1=<?php echo $tbl1; ?>&tbl2=<?php echo $tbl2; ?>&t=<?php echo $t; ?>", function(error, data) {
  if (error) throw error


  if(data.length < 5) {
    console.log("File empty")

    svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .attr("class", "legend")
        .text("no data found!");
  } else {

    svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .attr("class", "legend")
        .text("<?php echo $tbl1 . '/' .  $tbl2   . ' ' . date("j. F Y", strtotime( $t . ' days' ) );?>");  
  }

  // format the data
  data.forEach(function(d) {
      d.createdate1 = parseTime(d.createdate1);
      d.createdate2 = parseTime(d.createdate2);
      d.thevalue1 = +d.thevalue1;
      d.thevalue2 = +d.thevalue2;
  });

  min = d3.min(data, function(d) { return Math.min(d.thevalue1, d.thevalue2); })
  max = d3.max(data, function(d) { return Math.max(d.thevalue1, d.thevalue2); })
  
  if(min > 0) {
    min = 0;
  } else {
     min = min - 1;
  }

  max = max + 1;

  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.createdate1; }));
  y.domain([min,max]);

  // Add the valueline path.
  svg.append("path")
      .data([data])
      .attr("class", "line1")
      .attr("id", "line1")      
      .attr("d", valueline1);

  // Add the scatterplot
  svg.selectAll("dot")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot1")
      .attr("r", 1)
      .attr("cx", function(d) { return x(d.createdate1); })
      .attr("cy", function(d) { return y(d.thevalue1); });


  // Add the valueline2 path.
 /* */
  svg.append("path")
      .data([data])
      .attr("class", "line2")
      .attr("id", "line2") 
      .attr("d", valueline2);

  // Add the scatterplot
  svg.selectAll("dot")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot2")
      .attr("r", 1)
      .attr("cx", function(d) { return x(d.createdate2); })
      .attr("cy", function(d) { return y(d.thevalue2); });



  // Add the X Axis
  svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x).tickFormat(d3.timeFormat("%H:%M")))

  // Add the Y Axis
  svg.append("g")
      .call(d3.axisLeft(y));


  // Add the blue line title
  svg.append("text")
    .attr("x", 0)             
    .attr("y", height + margin.top + 10)    
    .attr("class", "legend-small pointer")       
    .on("click", function(){
      // Determine if current line is visible
      var activeLine = line1.activeLine ? false : true;
      var newOpacityLine = activeLine ? 1 : 0;
      // Hide or show the elements
      d3.select("#line1").style("opacity", newOpacityLine);
      // Update whether or not the elements are active
      line1.activeLine = activeLine;
    })
    .text("show/hide line 1");  


  // Add the blue line title
  svg.append("text")
    .attr("x", 120)             
    .attr("y", height + margin.top + 10)    
    .attr("class", "legend-small pointer")       
    .on("click", function(){
      // Determine if current line is visible
      var activeLine = line2.activeLine ? false : true;
      var newOpacityLine = activeLine ? 1 : 0;
      // Hide or show the elements
      d3.select("#line2").style("opacity", newOpacityLine);
      // Update whether or not the elements are active
      line2.activeLine = activeLine;
    })
    .text("show/hide line 2")


  // Add the blue line title
  svg.append("text")
    .attr("x", 240)             
    .attr("y", height + margin.top + 10)    
    .attr("class", "legend-small pointer")       
    .on("click", function(){
      // Determine if current line is visible
      var activeLine = line1.activeLine ? false : true;
      var newOpacityLine = activeLine ? 1 : 0;
      // Hide or show the elements
      d3.select("#line2").style("opacity", newOpacityLine);
      d3.select("#line1").style("opacity", newOpacityLine);
      // Update whether or not the elements are active
      line1.activeLine = activeLine;
    })
    .text("show/hide line 1 and 2");  

  // text label for the x axis
  svg.append("text")  
      .attr("class","legend-small")           
      .attr("transform",
            "translate(" + (width/2) + " ," + (height + margin.bottom - 10) + ")")
      .style("text-anchor", "middle")
      .text("time");

  // text label for the y axis
  svg.append("text")
      .attr("class","legend-small")
      .attr("transform", "rotate(-90)")
      .attr("y", 10 - margin.left)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("<?php echo $legend_y_axis; ?>");      

});
</script>
<script src="/fruit2/vendor/jquery-3.2.1.slim.min.js"></script>
<script src="/fruit2/vendor/bootstrap.min.4.1.3.js"></script>
</body>
</html>