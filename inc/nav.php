<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
  <a class="navbar-brand mr-auto mr-lg-0" href="#"> fruit charts, <small>timezone <?php echo date_default_timezone_get(); ?></small></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse">
        <ul class="navbar-nav ml-auto mr-12">

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown-demos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">open period</a>
            <div class="dropdown-menu" style="min-width:12.5rem">
              <form class="px-4 py-3" method="get" action="/fruit2/chart/" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" novalidate >

                  <div class="form-group row mb-1">
                    <label for="tbl" class="col-12 col-lg-4 col-form-label form-control-sm">sensor</label>
                    <div class="col-12 col-lg-8">
                      <select class="form-control form-control-sm" id="tbl" name="tbl">
                        <option value="te3">te3</option>
                        <option value="te7">te7</option>
                        <option value="fu3">fu3</option>
                        <option value="fu7">fu7</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row mb-1">
                    <label for="t1" class="col-12 col-lg-4 col-form-label form-control-sm">begin</label>
                    <div class="col-12 col-lg-8">
                      <select class="form-control form-control-sm" id="t1" name="t1">
                        <?php
                        for($days = 1; $days < 31; $days++) {
                          echo '<option value="-' . $days . '">' . $days . ' days ago</option>';
                        }
                        ?>
                        </select>
                    </div>
                  </div>

                  <div class="form-group row mb-2">
                    <label for="t2" class="col-12 col-lg-4 col-form-label form-control-sm">end</label>
                    <div class="col-12 col-lg-8">
                      <select class="form-control form-control-sm" id="t2" name="t2">
                        <?php
                        for($days = 0; $days < 31; $days++) {
                          echo '<option value="-' . $days . '">' . $days . ' days ago</option>';
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row mb-1">
                    <div class="col-12 form-control-sm">
                    <button type="submit" class="btn btn-primary btn-sm btn-block">go</button>
                  </div>
                  </div>
              </form>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown-demos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">yesterday/week</a>
            <div class="dropdown-menu" aria-labelledby="dropdown-demos">
              <h6 class="dropdown-header">sensor te3</h6>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=te3&t1=-30&t2=0">last month</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=te3&t1=-7&t2=0">last week</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=te3&t1=-1&t2=0">yesterday</a>
              <h6 class="dropdown-header">sensor te7</h6>
               <a class="dropdown-item" href="/fruit2/chart/?tbl=te7&t1=-30&t2=0">last month</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=te7&t1=-7&t2=0">last week</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=te7&t1=-1&t2=0">yesterday</a>
              <h6 class="dropdown-header">sensor fu3</h6>
               <a class="dropdown-item" href="/fruit2/chart/?tbl=fu3&t1=-30&t2=0">last month</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=fu3&t1=-7&t2=0">last week</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=fu3&t1=-1&t2=0">yesterday</a>
              <h3 class="dropdown-header">sensor fu7</h3>
               <a class="dropdown-item" href="/fruit2/chart/?tbl=fu7&t1=-30&t2=0">last month</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=fu7&t1=-7&t2=0">last week</a>
              <a class="dropdown-item" href="/fruit2/chart/?tbl=fu7&t1=-1&t2=0">yesterday</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown-demos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">compare sensors </a>
            <div class="dropdown-menu" aria-labelledby="dropdown-demos">
              <h3 class="dropdown-header">compare te3/te7</h3>
              <?php
              for($days = 1; $days < 8; $days++) {
                $datum = date("j.m.Y", strtotime(-$days . ' days'));
                echo '<a class="dropdown-item" href="/fruit2/chart-multiline/?tbl1=te3&tbl2=te7&t=-' . $days . '">' . $datum  . '</a>';
              }
              ?>
              <h3 class="dropdown-header">compare fu3/fu7</h3>
              <?php
              for($days = 1; $days < 8; $days++) {
                $datum = date("j.m.Y", strtotime(-$days . ' days'));
                echo '<a class="dropdown-item" href="/fruit2/chart-multiline/?tbl1=fu3&tbl2=fu7&t=-' . $days . '">' . $datum  . '</a>';
              }
              ?>
            </div>
          </li>
        </ul>
      </div>
</nav>